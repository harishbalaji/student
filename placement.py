from tkinter import *
import  sqlite3
import tkinter.messagebox as tm
#import placement


def MarkDescending():
    conn = sqlite3.connect('user.db')
    with conn:
        curs = conn.cursor()
        curs.execute('select * from student ORDER BY Apptitude_Mark ')
        conn.commit()



def show_entry_fields():
    conn = sqlite3.connect('user.db')
    curs = conn.cursor()
    show=curs.execute('select* from student')
    for data in show:
        list1.insert(END,data)
    """list1.delete(0, END)
    for row in placement.show_entry_fields():
        list1.insert(END, row)
    row = curs.fetchall()
    return row"""


def Delete():
    #Name = e1.get()
    Rollnumber = e2.get()
    #DOB = e3.get()
    #Department = e4.get()
    #Year_of_Admission = e5.get()
    #Skill_Set = e6.get()
    #Apptitude_Mark = e7.get()
    conn = sqlite3.connect('user.db')
    with conn:
        curs = conn.cursor()
        show=curs.execute("delete from student where Rollnumber=?",(Rollnumber,))
        for data in show:
            list1.insert(END, data)
        conn.commit()



def Update():
    Name = e1.get()
    Rollnumber = e2.get()
    DOB = e3.get()
    Department = e4.get()
    Year_of_Admission = e5.get()
    Skill_Set = e6.get()
    Apptitude_Mark = e7.get()
    CGPA=e8.get()
    conn = sqlite3.connect('user.db')
    with conn:
        curs = conn.cursor()
        curs.execute("update student set DOB=?,Department=?,Year_of_Admission=?,Skill_Set=?,Apptitude_Mark=?,CGPA=? where Rollnumber=?",(DOB,Department,Year_of_Admission,Skill_Set,Apptitude_Mark,CGPA,Rollnumber,))

def desc():
    conn = sqlite3.connect('user.db')
    #conn = sqlite3.connect('asc.db')
    with conn:
        curs= conn.cursor()
        curs.execute("select * from student order by Apptitude_Mark DESC")
        show=curs.fetchall()
        for i in show:
            print(i)
        curs.execute("delete from student")
        for i in show:
           curs.execute('insert into student(Name,Rollnumber,DOB,Department,Year_of_Admission,Skill_Set,Apptitude_Mark,CGPA) values(?,?,?,?,?,?,?,?)',(i))

def CGPA():
    conn = sqlite3.connect('user.db')
    #conn = sqlite3.connect('asc.db')
    with conn:
        curs= conn.cursor()
        curs.execute("select * from student order by CGPA DESC")
        show=curs.fetchall()
        for i in show:
            print(i)
        curs.execute("delete from student")
        for i in show:
           curs.execute('insert into student(Name,Rollnumber,DOB,Department,Year_of_Admission,Skill_Set,Apptitude_Mark,CGPA) values(?,?,?,?,?,?,?,?)',(i))

def skillwise():
    conn = sqlite3.connect('user.db')
    # conn = sqlite3.connect('asc.db')
    with conn:
        curs = conn.cursor()
        curs.execute("select * from student where Skill_Set='java,c'")
        show = curs.fetchall()
        for i in show:
            print(i)
            list1.insert(END, i)

        #for i in show:
         #   curs.execute('insert into student(Name,Rollnumber,DOB,Department,Year_of_Admission,Skill_Set,Apptitude_Mark,CGPA) values(?,?,?,?,?,?,?,?)',(i))


"""def returnEntry(arg=None):
    Gets the result from Entry and return it to the Label

    result = myEntry.get()
    resultLabel.config(text=result)
    myEntry.delete(0, END)"""


master = Tk()
Label(master,  text="Name").grid(row=0)
Label(master, text="Rollnumber").grid(row=1)
Label(master, text="DOB").grid(row=2)
Label(master, text="Department").grid(row=3)
Label(master, text="Year_of_Admission").grid(row=4)
Label(master, text="Skill_Set").grid(row=5)
Label(master, text="Apptitude_Mark").grid(row=6)
Label(master, text="CGPA").grid(row=7)

e1 = Entry(master)
e2 = Entry(master)
e3= Entry(master)
e4= Entry(master)
e5= Entry(master)
e6= Entry(master)
e7= Entry(master)
e8= Entry(master)

e1.grid(row=0, column=1)
e2.grid(row=1, column=1)
e3.grid(row =2,column=1)
e4.grid(row =3,column=1)
e5.grid(row =4,column=1)
e6.grid(row =5,column=1)
e7.grid(row =6,column=1)
e8.grid(row =7,column=1)

list1=Listbox(master,height=20,width=59)
list1.grid(row=1,column=3, rowspan=6, columnspan=2)

scrl=Scrollbar(master)
scrl.grid(row=1,column=2, sticky='ns',rowspan=6)

list1.configure(yscrollcommand=scrl.set)
scrl.configure(command=list1.yview)

"""resultLabel = Label(master, text = "")
resultLabel.pack(fill=X)"""
Button(master, text='Quit', command=master.quit).grid(row=8, column=0, sticky=W, pady=4)
Button(master, text='Show', command=show_entry_fields).grid(row=8, column=1, sticky=W, pady=4)
#Button(master, text='Submit', command=submit).grid(row=7, column=2, sticky=W, pady=4)
Button(master, text='Update', command=Update).grid(row=8, column=2, sticky=W, pady=4)
Button(master, text='Order', command=desc).grid(row=9, column=0, sticky=W, pady=4)
Button(master, text='Delete', command=Delete).grid(row=9, column=2, sticky=W, pady=4)
Button(master, text='Skillwise', command=skillwise).grid(row=9, column=1, sticky=W, pady=4)
mainloop( )